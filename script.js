
// task 1


       
        const btn = document.getElementById('btn-click');
        const section = document.getElementById('content');

        
        btn.addEventListener('click', function() {
            
            const text = document.createElement('p');
            
            text.textContent = 'New Paragraph';
        
            section.append(text);
        });
    

        // task 2


   
        const section1 = document.getElementById('content');
        const footer = document.querySelector('footer');

      
        const button = document.createElement('button');
        button.id = 'btn-input-create';
        button.textContent = 'Create Input';
        
        footer.before(button, footer);

        button.addEventListener('click', function() {
          
            const newInput = document.createElement('input');
            
            newInput.type = 'text';
            newInput.placeholder = 'text';
            newInput.name = 'input';
           
            section.before(newInput, button);
        });
